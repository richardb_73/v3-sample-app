FactoryGirl.define do
  factory :user do
    name     "Richard Blakemore"
    email    { Faker::Internet.email }
    password "foobar"
    password_confirmation "foobar"
  end
end