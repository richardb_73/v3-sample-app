require 'rails_helper'

describe "Authentication" do
let(:title) { "Ruby on Rails Tutorial Sample App" }
  subject { page }

  describe "login page" do
    before { visit login_path }
    describe "with invalid information" do
        before { click_button "Log in" }
  
        it { should have_title("#{title}") }
        it { should have_content('Invalid email/password combination') }
        
      describe "after visiting another page" do
        before { click_link "Home" }
        it { should_not have_content('Invalid email/password combination') }
      end
    end
    
    describe "with valid information" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in "Email",    with: user.email.upcase
        fill_in "Password", with: user.password
        click_button "Log in"
      end

      it { should have_title("#{title} | #{user.name}") }
      it { should have_link('Profile', href: user_path(user)) }
      it { should have_link('Log out', href: logout_path) }
      it { should_not have_link('Log in', href: login_path) }
      
      describe "followed by signout" do
        before { click_link "Log out" }
        it { should have_link('Log in') }
      end
    end
  end
end