require 'rails_helper'
require 'capybara'


describe "User pages" do
let(:title) { "Ruby on Rails Tutorial Sample App" }
  subject { page }
  
    describe "profile page" do
      let(:user) { FactoryGirl.create(:user) }
      before { visit user_path(user) }
  
      it { should have_selector('h1',    text: user.name) }
      it { should have_title("#{title} | #{user.name}") }
    end
    
describe "signup" do

    before { visit signup_path }

    let(:submit) { "Create my account" }

    describe "with invalid information" do
      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end
    end

    describe "with valid information" do
      before do
      @user = User.new(name: Faker::Name.name, email: Faker::Internet.email,
                       password: "foobar", password_confirmation: "foobar")
      end
      before do
        fill_in "Name",         with: @user.name 
        fill_in "Email",        with: @user.email
        fill_in "Password",     with: @user.password
        fill_in "Confirmation", with: @user.password_confirmation
      end

      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end
    end
  end
end